
ProBook6475fc
=============

Linux kernel module to control the CPU fan of a HP ProBook 6475b laptop.

As HP's build in fan controlling is quit conservative at the lower temperature
range, this module controls the vantilation less restrictive as the following
table shows:

| fan speed              |  off  | 2350rpm | 2740rpm | 3210rpm | 3750rpm | 4280rpm |
| :---                   | :---: |  :---:  |  :---:  |  :---:  |  :---:  |  :---:  |
| step up temp. HP       |  51°C |   58°C  |   65°C  |   72°C  |   79°C  |    -    |
| step down temp. HP     |   -   |   43°C  |   50°C  |   57°C  |   64°C  |   71°C  |
| step up temp. module   |  58°C |   64°C  |   68°C  |   72°C  |   75°C  |    -    |
| step down temp. module |   -   |   48°C  |   58°C  |   64°C  |   68°C  |   72°C  |

Furthermore the CPU temperature is a bit low-pass filtered to restrain hypersensitive
temperature sensors.


Restrictions
------------
The BIOS is checked regarding system vendor and product name, so that the module
will refuse to work on other hardware platforms than HP ProBook 6475b.  

The module just allows to set the verbosity level of kernel message output (0..3,
default=0) and the polling interval of processing (1000..10000ms, default=3000ms).
The temperature thresholds and fan speeds are hard-coded and have to be changed
within the source code if required.


Example usage on Debian 8
-------------------------
- **Generate source code documentation:** `doxygen ProBook6475fc.doxyfile`
- **Generate deb package:** `sudo dkms.sh`
- **Install deb package:** `sudo dkms -i *.deb`
- **View module information:** `sudo modinfo pb6475fc`
- **Load module manually:** `sudo modprobe pb6475fc`, or `sudo insmod pb6475fc.ko`
- **Unload module manually:** `sudo rmmod pb6475fc`
- **Load module automatically on system start:**
  1. *Add module name to /etc/modules:* `sudo sh -c "echo '\n'pb6475fc >> /etc/modules"`
  2. *Add module options to /etc/modprobe.d/pb6475fc.conf:* `sudo sh -c "echo options pb6475fc verbose=1 interval=5000 > /etc/modprobe.d/pb6475fc.conf"`
- **Review module output**: `dmesg | grep pb6475fc`


Credits
-------
ProBook6475fc was inspired by the linux kernel module
[acerhdf](http://lxr.free-electrons.com/source/drivers/platform/x86/acerhdf.c).
