/**
 * @file
 * @mainpage
 *
 * @code
 * Module name    :  pb6475fc (linux kernel module)
 *
 * Purpose        :  Linux kernel module to control the CPU fan of a Hewlett-Packard
 *                   ProBook 6475b laptop.
 *
 * Load module    :  insmod pb6475fc verbose=[0..3] interval=[1000...10000]
 *
 * Unload module  :  rmmod  pb6475fc
 *
 * Parameters     :  /sys/module/pb6475fc/parameters/interval
 *                :  /sys/module/pb6475fc/parameters/verbose
 *
 * Documentation  :  http://www.tldp.org/LDP/lkmpg/2.6/html/lkmpg.html
 *                :  http://www.kernel.org/doc/Documentation/workqueue.txt
 *                :  http://lxr.free-electrons.com/source/Documentation/workqueue.txt
 *                :  http://www.kernel.org/doc/Documentation/driver-model/platform.txt
 *                :  http://lxr.free-electrons.com/source/Documentation/driver-model/platform.txt
 *
 * Author         :  Marcel Schlottmann
 *
 * License        :  GPL
 *
 * Last change    :  2016-08-23
 *
 * @endcode
 * @n
 *
 * @dot
 * digraph "Dynamic Loading, Platform Driver" {
 *    node [shape="box"];
 *    n01  [label="kernel\ndynamic loading", style="bold"];
 *    n02  [label="pb6475fc_init()", URL="\ref pb6475fc_init"];
 *    n03  [label="pb6475fc_exit()", URL="\ref pb6475fc_exit"];
 *    n11  [label="kernel\nplatform driver", style="bold"];
 *    n12  [label="pb6475fc_shutdown()",   URL="\ref pb6475fc_shutdown"];
 *    n01 -> n02;
 *    n01 -> n03 [style="dashed"];
 *    n11 -> n12;
 * }
 * @enddot
 * @n
 *
 * @dot
 * digraph "Kernel Power Management" {
 *    node [shape="box"];
 *    n01  [label="kernel\npower management", style="bold"];
 *    n02  [label="pb6475fc_suspend()", URL="\ref pb6475fc_suspend"];
 *    n03  [label="pb6475fc_resume()",  URL="\ref pb6475fc_resume"];
 *    n04  [label="pb6475fc_freeze()",  URL="\ref pb6475fc_freeze"];
 *    n05  [label="pb6475fc_restore()", URL="\ref pb6475fc_restore"];
 *    n01 -> {n02 ,n03 ,n04 ,n04, n05};
 * }
 * @enddot
 * @n
 *
 * @dot
 * digraph "Kernel Parameter, Work Queue" {
 *    node [shape="box"];
 *    n01  [label="kernel\nparameter", style="bold"];
 *    n02  [label="param_set_verbose()", URL="\ref param_set_verbose"];
 *    n03  [label="param_set_interval()", URL="\ref param_set_interval"];
 *    n11  [label="kernel\nwork queue", style="bold"];
 *    n12  [label="fan_control()", URL="\ref fan_control"];
 *    n01 -> {n02, n03};
 *    n11 -> n12;
 * }
 * @enddot
 * @n
*/
#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/workqueue.h>
#include <linux/jiffies.h>
#include <linux/acpi.h>
#include <linux/dmi.h>


/* ************************************************************************** *
 *                            Function prototypes
 * ************************************************************************** */

static int param_set_interval(const char *, const struct kernel_param *);
static int param_set_verbose(const char *, const struct kernel_param *);

static void pb6475fc_shutdown(struct platform_device *);
static int pb6475fc_suspend(struct device *);
static int pb6475fc_resume(struct device *);
static int pb6475fc_freeze(struct device *);
static int pb6475fc_restore(struct device *);

static int check_system(const char *, const char *);
static int register_platform(void);
static void unregister_platform(void);
static int takeover_fan_control(void);
static int reassign_fan_control(void);
static int schedule_fan_control(void);

static inline int EC_get_fan_speed(u8 *);
static inline int EC_set_fan_speed(u8);
static inline int EC_get_thermal_zone(u8 *);
static inline int EC_set_thermal_zone(u8);
static inline int EC_get_CPU_temperature(u8 *);
static inline int EC_set_zone_temperature(u8);

static void fan_control(struct work_struct *);
static int verify_temperature(u8, u8 *);
static int verify_fan_speed(u8);
static u8 get_CPU_temperature(void);
static int set_fan_state(u8);

static int __init pb6475fc_init(void);
static void __exit pb6475fc_exit(void);


/* ************************************************************************** *
 *                           Macro definitions
 * ************************************************************************** */

/**
 * EC register address to select a thermal zone.
 */
#define CRZNREG 0x22

/**
 * EC register address to pretend a fake temperature to the select a thermal zone.
 */
#define TEMPREG 0x26

/**
 * EC register address to get the current fan speed.
 */
#define FRDCREG 0x2E

/**
 * EC register address to request a fan speed.
 */
#define FTGCREG 0x2F

/**
 * EC register address to get the current device temperature of the selected thermal zone.
 */
#define DTMPREG 0x32

/**
 * EC register value to select the CPU thermal zone (CPU Zone).
 */
#define CPUZ 0x01

/**
 * EC register value to cheat the EC with a fake CPU temperature (1Fh = 31d = 31°C)
 */
#define CPUZCHEATTEMP 0x1F

/**
 * EC register value to reset the EC to real CPU temperature (00h = reset)
 */
#define CPUZRESETTEMP 0x00

/**
 * Number of supported fan speed states (0 := fan off, .., MAXFANSTATE := max. fan speed).
 */
#define MAXFANSTATE 5

/**
 * Minimum polling interval in milliseconds for fan controlling.
 */
#define MINPOLLINT 1000

/**
 * Maximum polling interval in milliseconds for fan controlling.
 */
#define MAXPOLLINT 10000

/**
 * Maximum supported verbose level for kernel message output.
 */
#define MAXVERBOSE 3

/**
 * Delay time in milliseconds to wait for error handling in case the temperature
 * cannot be read out properly from EC.
 */
#define VERIFYTEMPDELAY 10000

/**
 * Zone temperature in °C to be set in case the current temperature could not
 * be read out properly.
 */
#define EMERGENCYTEMP 70

/**
 * Critical Zone temperature level in °C at which the driver resets fan controlling
 * back to the EC.
 */
#define CRITICALTEMP 80

/**
 * Delay time in milliseconds to wait until the requested and measured fan speed
 * are compared for error handling.
 */
#define VERIFYSPEEDDELAY 15000

/**
 * Maximum of accepted deviation between requested and measured fan speed for
 * error handling (4 Digits ~= 100rpm).
 */
#define MAXSPEEDDEVIATION 4


/* ************************************************************************** *
 *                            Module parameters
 * ************************************************************************** */

/**
 * Parameter to set the verbose level of kernel message output
 */
static uint verbose = 0;
MODULE_PARM_DESC(verbose, " Enable verbose kernel message output [0..3], default = 0");
static struct kernel_param_ops verbose_cb_ops = {
		.flags = 0, .set = param_set_verbose, .get = param_get_uint, .free = NULL };
module_param_cb(verbose, &verbose_cb_ops, &verbose, 0600);

/**
 * Parameter to set the polling interval for temperature checks
 */
static uint interval = 3000;
MODULE_PARM_DESC(interval, " Polling interval for temperature check [1000..10000ms], default = 3000ms");
static struct kernel_param_ops interval_cb_ops = {
		.flags = 0, .set = param_set_interval, .get = param_get_uint, .free = NULL };
module_param_cb(interval, &interval_cb_ops, &interval, 0600);


/* ************************************************************************** *
 *                        Module (global) variables
 * ************************************************************************** */

/**
 * Current state of fan controlling.
 *
 * Range: 0 (= off) .. MAXFANSTAT (= maximum cooling)
 */
static u8 current_fan_state = 0;

/**
 * @var const u8 fan_upshift_temps[MAXFANSTATE + 1]
 * Temperature at which the current fan state shall be increased
 */
/**
 * @var const u8 fan_downshift_temps[MAXFANSTATE + 1]
 * Temperature at which the current fan state shall be decreased
 */
/**
 * @var const u8 fan_cycles[MAXFANSTATE + 1]
 * Cycle the fan shall be set to at the current fan state.
 */
                                   /* current_fan_state:   0,    1,    2,    3,    4,    5 */
static const u8 fan_upshift_temps[MAXFANSTATE + 1] =   {  58,   64,   68,   72,   75, 255 };
static const u8 fan_downshift_temps[MAXFANSTATE + 1] = {   0,   48,   58,   64,   68,  72 };
static const u8 fan_cycles[MAXFANSTATE + 1] =          { 255,  104,   89,   76,   65,  57 };
                                           /* Fan speed: off, 2350, 2740, 3210, 3750, 4280 [rpm]*/

/**
 * Flag to start/stop fan controlling
 */
static bool fan_controlling = 0;

/**
 * Flag to reset the temperature validation function
 */
static bool reset_temp_verification = 0;

/**
 *  Flag to reset fan speed validation function
 */
static bool reset_speed_verification = 0;

/**
 * Delayed work queue to cyclically trigger the function fan_control().
 */
static DECLARE_DELAYED_WORK(pb6475fc_wq, fan_control);

/**
 * Platform device to handle suspend to RAM, suspend to disk and shutdown events.
 */
static struct platform_device *pb6475fc_pt_dev = NULL;

/**
 * Platform device power management callback functions
 */
static const struct dev_pm_ops pb6475fc_pm_ops = {
		.suspend = pb6475fc_suspend,
		.resume  = pb6475fc_resume,
		.freeze  = pb6475fc_freeze,
		.restore = pb6475fc_restore,
		};

/**
 * Platform device driver callback functions
 */
static struct platform_driver pb6475fc_pt_drv = {
		.driver = {
				.name = "pb6475fc",
				.owner = THIS_MODULE,
				.suppress_bind_attrs = 1,
				.pm = &pb6475fc_pm_ops,
			},
		.shutdown = pb6475fc_shutdown,
		};


/* ************************************************************************** *
 *                      Parameter callback function(s)
 * ************************************************************************** */

/**
 * Parameter callback function that set/updates the parameter 'interval'.
 *
 * The function is called automatically by the kernel while the module is loaded
 * with the parameter 'interval=x' or after the module has fully initialized and
 * the parameter is changed via the /sys-filesystem:
 * 'echo 1 > /sys/module/thinkpadfc/parameters/interval'
 *
 * <b>Global variables</b>@n
 * @link interval interval @endlink <c>[in,out]</c>
 */
static int param_set_interval(const char *val, const struct kernel_param *kp)
{
	int err;

	/* Update parameter internally with kernel system function */
	err = param_set_uint(val, kp);
	if (err) {
		pr_err("Parameter 'interval' could not be updated, it remains at %ums!\n", interval);
		goto exit;
	}

	/* Check and if necessary limit the polling interval within MIN/MAX boarders */
	interval = (interval < MINPOLLINT) ? MINPOLLINT :
			((interval > MAXPOLLINT) ? MAXPOLLINT : interval);

	pr_info("Polling interval set to %ums\n", interval);

exit:
	return err;
}

/**
 * Parameter callback function that set/updates the parameter 'verbose'.
 *
 * The function is called automatically by the kernel while the module is loaded
 * with the parameter 'verbose=x' or after the module has fully initialized and
 * the parameter is changed via the /sys-filesystem:
 * 'echo 1 > /sys/module/thinkpadfc/parameters/verbose'
 *
 * <b>Global variables</b>@n
 * @link verbose verbose @endlink <c>[in,out]</c>
 */
static int param_set_verbose(const char *val, const struct kernel_param *kp)
{
	int err;

	/* Update parameter internally by kernel system function */
	err = param_set_uint(val, kp);

	if (err) {
		pr_err("Parameter 'verbose' could not be updated, it remains at %u!\n", verbose);
		goto exit;
	}

	/* Check and if necessary limit the verbose level to MAXIMUM boarder */
	if (verbose > MAXVERBOSE) verbose = MAXVERBOSE;

	if (verbose) {
		pr_info("Verbose level set to %u\n", verbose);
	} else {
		pr_info("Verbose message output disabled\n");
	}

exit:
	return err;
}


/* ************************************************************************** *
 *                   Platform Device call back functions
 * ************************************************************************** */

/**
 * Platform device callback function to stop and reassign fan controlling to the
 * embedded controller before the system shuts down.
 *
 * The function is called by the power management before the computer reboots or
 * shuts down - in this case pb6475fc_exit() is usually not called directly by
 * the kernel.
 *
 * <b>Global variables</b>@n
 * @link fan_controlling fan_controlling @endlink <c>[in,out]</c>
 * @link pb6475fc_wq pb6475fc_wq @endlink <c>[out]</c>
 */
static void pb6475fc_shutdown(struct platform_device *dev)
{
	/* Reassigning fan control is not required if the processing has stopped
	 * due to error or couldn't started at all while module initialization.
	 */
	if (fan_controlling) {
		cancel_delayed_work_sync(&pb6475fc_wq);

		if (reassign_fan_control())
			pr_emerg("Fan control reassigned to EC failed while shutdown!\n");
		else
			pr_notice("Fan control reassigned to EC while shutdown\n");

		fan_controlling = 0;
	}

	return;
}

/**
 * Platform device callback function to reassign fan controlling to the
 * embedded controller before the system suspends to RAM (ACPI S3 mode).
 *
 * The function is called by the power management system before the computer
 * changes to S3 state (suspend to RAM).
 *
 * <b>Global variables</b>@n
 * @link fan_controlling fan_controlling @endlink <c>[in]</c>
 * @link pb6475fc_wq pb6475fc_wq @endlink <c>[out]</c>
 */
static int pb6475fc_suspend(struct device *dev)
{
	int err = 0;

	/* Reassigning fan control is not required if the processing has stopped
	 * due to error or couldn't started at all while module initialization.
	 */
	if (fan_controlling) {
		cancel_delayed_work_sync(&pb6475fc_wq);

		err = reassign_fan_control();
		if (err)
			pr_err("Fan control reassigned to EC failed before suspending to RAM!\n");
		else
			pr_notice("Fan control reassigned to EC before suspending to RAM\n");
	}

	return err;
}

/**
 * Platform device callback function to take over fan controlling from the
 * embedded controller after the system resumes from RAM (ACPI S3 mode).
 *
 * The function is called by the power management system after the computer
 * wakes up form S3 state (suspend to RAM).
 *
 * <b>Global variables</b>@n
 * @link fan_controlling fan_controlling @endlink <c>[in,out]</c>
 * @link reset_speed_verification reset_speed_verification @endlink <c>[out]</c>
 * @link reset_temp_verification reset_temp_verification @endlink <c>[out]</c>
 */
static int pb6475fc_resume(struct device *dev)
{
	int err = 0;

	/* Taking over and scheduling fan control is not required if the processing has
	 * stopped due to error or couldn't started at all while module initialization.
	 */
	if (fan_controlling) {

		err = takeover_fan_control();
		if (err) {
			pr_err("Fan control taken over form EC failed after resuming from RAM!\n");
			fan_controlling = 0;
			goto exit;
		}
		else {
			pr_notice("Fan control taken over from EC after resuming from RAM\n");
		}

		err = schedule_fan_control();
		if (err) {
			pr_err("Scheduling fan control failed after resuming from RAM!\n");

			if (reassign_fan_control())
				pr_emerg("Fan control reassigned to EC failed!\n");
			else
				pr_notice("Fan control reassigned to EC\n");

			fan_controlling = 0;
			goto exit;
		}

		/* Reset verification functions while module initialization or
		 * after suspend to RAM/disk.
		 */
		reset_speed_verification = 1;
		reset_temp_verification = 1;
	}

exit:
	return err;
}

/**
 * Platform device callback function to reassign fan controlling to the
 * embedded controller before the system suspends to disk (ACPI S4 mode).
 *
 * The function is called by the power management system before the computer
 * creates a hibernation image.
 *
 * <b>Global variables</b>@n
 * @link fan_controlling fan_controlling @endlink <c>[in]</c>
 * @link pb6475fc_wq pb6475fc_wq @endlink <c>[out]</c>
 */
static int pb6475fc_freeze(struct device *dev)
{
	int err = 0;

	/* Reassigning control is not required if the processing has stopped
	 * due to error or couldn't started at all while module initialization.
	 */
	if (fan_controlling) {
		cancel_delayed_work_sync(&pb6475fc_wq);

		err = reassign_fan_control();
		if (err)
			pr_err("Fan control reassigned to EC failed before suspending to disk!\n");
		else
			pr_notice("Fan control reassigned to EC before suspending to disk\n");
	}

	return err;
}

/**
 * Platform device callback function to take over fan controlling from the
 * embedded controller after the system restores from disk (ACPI S4 mode).
 *
 * The function is called by the power management system after the computer
 * restores the contents from the hibernation image.
 *
 * <b>Global variables</b>@n
 * @link fan_controlling fan_controlling @endlink <c>[in,out]</c>
 * @link reset_speed_verification reset_speed_verification @endlink <c>[out]</c>
 * @link reset_temp_verification reset_temp_verification @endlink <c>[out]</c>
 */
static int pb6475fc_restore(struct device *dev)
{
	int err = 0;

	/* Taking over and scheduling fan control is not required if the processing has
	 * stopped due to error or couldn't started at all while module initialization.
	 */
	if (fan_controlling) {

		err = takeover_fan_control();
		if (err) {
			pr_err("Fan control taken over form EC failed after restoring from disk!\n");
			fan_controlling = 0;
			goto exit;
		}
		else {
			pr_notice("Fan control taken over from EC after restoring from disk\n");
		}

		err = schedule_fan_control();
		if (err) {
			pr_err("Scheduling fan control failed after restoring from disk!\n");

			if (reassign_fan_control())
				pr_emerg("Fan control reassigned to EC failed!\n");
			else
				pr_notice("Fan control reassigned to EC\n");

			fan_controlling = 0;
			goto exit;
		}

		/* Reset verification functions while module initialization or
		 * after suspend to RAM/disk.
		 */
		reset_speed_verification = 1;
		reset_temp_verification = 1;
	}

exit:
	return err;
}


/* ************************************************************************** *
 *                System initialization and exit functions
 * ************************************************************************** */

/**
 * Function to verify compatibility between the system hardware and module.
 *
 * The function checks if the system vendor and product name requested via
 * function parameter matches the corresponding entries stored in the System
 * Management BIOS (SMBIOS).
 *
 * @param[in]  vendor   System vendor, i.e. Hewlett-Packard
 * @param[in]  name     Product name of the system, i.e. HP ProBook 675b
 *
 * @retval  0  All entries match
 * @retval -1  At least one entry doesn't match
 */
static int check_system(const char *vendor, const char *name)
{
	const char *dmi_vendor, *dmi_name;

	/* Read system vendor and product name out of the SMBIOS */
	dmi_vendor  = dmi_get_system_info(DMI_SYS_VENDOR);
	dmi_name    = dmi_get_system_info(DMI_PRODUCT_NAME);

	/* Compare requested vendor and name with SMBIOS entries. */
	if (strlen(vendor) != strlen(dmi_vendor) || strcmp(vendor, dmi_vendor)) {
		pr_err("Vendor %s expected, but %s detected\n", vendor, dmi_vendor);
		goto error;
	}

	if (strlen(name) != strlen(dmi_name) || strcmp(name, dmi_name))
	{
		pr_err("%s expected, but %s detected\n", name, dmi_name);
		goto error;
	}

	if (verbose > 2)
		pr_info("\t\tSystem check passed\n");

	return 0;
error:
	return -1;
}

/**
 * Function to register a platform driver and a platform device while module
 * initialization.
 *
 * These components are required to get access to the system's power management
 * to be able to handle suspend to RAM, hibernate and shutdown states.
 *
 * <b>Global variables</b>@n
 * @link pb6475fc_pt_dev pb6475fc_pt_dev @endlink <c>[in,out]</c>
 * @link pb6475fc_pt_drv pb6475fc_pt_drv @endlink <c>[out]</c>
 * @link verbose verbose @endlink <c>[in]</c>
 */
static int register_platform(void)
{
	int err = 0;

	/* Register platform driver */
	err = platform_driver_register(&pb6475fc_pt_drv);
	if (err) {
		pr_err("Platform driver registration failed!\n");
		goto exit;
	}
	if (verbose > 2)
		pr_info("\t\tPlatform driver registration passed\n");


	/* Register platform device */
	pb6475fc_pt_dev = platform_device_register_simple("pb6475fc", -1, NULL, 0);
	if (IS_ERR(pb6475fc_pt_dev)) {
		pr_err("Platform device registration failed!\n");
		platform_driver_unregister(&pb6475fc_pt_drv);
		pb6475fc_pt_dev = NULL;
		err = -ENODEV;
		goto exit;
	}
	if (verbose > 2)
		pr_info("\t\tPlatform device registration passed\n");

exit:
	return err;
}

/**
 * Function to unregister the platform device and driver while module unloading.
 *
 * <b>Global variables</b>@n
 * @link pb6475fc_pt_dev pb6475fc_pt_dev @endlink <c>[in,out]</c>
 * @link pb6475fc_pt_drv pb6475fc_pt_drv @endlink <c>[out]</c>
 * @link verbose verbose @endlink <c>[in]</c>
 */
static void unregister_platform(void)
{
	if (pb6475fc_pt_dev != NULL) {
		/* Remove the platform device */
		platform_device_unregister(pb6475fc_pt_dev);
		pb6475fc_pt_dev = NULL;
		if (verbose > 2)
			pr_info("\t\tPlatform device deregistered\n");

		/* Remove the platform driver */
		platform_driver_unregister(&pb6475fc_pt_drv);
		if (verbose > 2)
			pr_info("\t\tPlatform driver deregistered\n");
	}

	return;
}

/**
 * Function to take over fan control from the embedded controller (EC).
 *
 * This is done by switching on the fan to a middle speed step and setting the
 * CPU temperature to a low level value, so that the EC never tries to changes
 * the fan speed by its own.
 */
static int takeover_fan_control(void)
{
	int fan_err, zone_err = 0;
	u8 thermal_zone;

	/* Check, if the CPU fan can be controlled by this module before the EC
	 * controlling is bypassed.
	 */
	fan_err = set_fan_state((MAXFANSTATE + 1) / 2);
	if (fan_err) {
		pr_err("CPU fan could not be set to %u!\n", (MAXFANSTATE + 1) / 2);
		goto exit;
	}

	/* Check, if the correct thermal zone is selected. */
	zone_err = EC_get_thermal_zone(&thermal_zone);
	if (zone_err) {
		pr_err("Thermal zone could not be read!\n");
		goto exit;
	}

	/* If necessary, set the thermal zone to CPU. */
	if (thermal_zone != CPUZ) {
		zone_err = EC_set_thermal_zone(CPUZ);
		if (zone_err) {
			pr_err("Thermal zone could not be set to CPUZ!\n");
			goto exit;
		}
	}

	/* Set the CPU zone temperature to a fake value to cheat the EC. */
	zone_err = EC_set_zone_temperature(CPUZCHEATTEMP);
	if (zone_err) {
		pr_err("CPU zone temperature could not be set to %u°C!\n", CPUZCHEATTEMP);
		goto exit;
	}

exit:
	if (zone_err) {
		fan_err = set_fan_state(0);
		if (fan_err) {
			pr_err("CPU fan could not be reset!\n");
		}
	}

	return fan_err ? fan_err : zone_err;
}

/**
 * Function to reassign fan controlling to the embedded controller.
 *
 * This is done by resetting the CPU zone temperature register for the EC and
 * switching off the fan.
 */
static int reassign_fan_control(void)
{
	int zone_err, fan_err = 0;
	u8 thermal_zone;

	/* Check, if the correct thermal zone is selected. */
	zone_err = EC_get_thermal_zone(&thermal_zone);
	if (zone_err) {
		pr_err("Thermal zone could not be read!\n");
		goto exit;
	}

	/* If necessary, set the thermal zone to CPU zone. */
	if (thermal_zone != CPUZ) {
		zone_err = EC_set_thermal_zone(CPUZ);
		if (zone_err) {
			pr_err("Thermal zone could not be set to CPUZ!\n");
			goto exit;
		}
	}

	/* Reset the CPU zone temperature for the EC */
	zone_err = EC_set_zone_temperature(CPUZRESETTEMP);
	if (zone_err) {
		pr_err("CPU zone temperature could not be reset to %u!\n", CPUZRESETTEMP);
		goto exit;
	}

exit:
	/* In case of a thermal zone error, set the fan speed to the second highest
	 * level.
	 */
	if (zone_err) {
		fan_err = set_fan_state(MAXFANSTATE - 1);
		if (fan_err) {
			pr_err("CPU fan could not be set to emergency state %u\n", MAXFANSTATE - 1);
		}
	} else {
		/* Switch off fan */
		fan_err = set_fan_state(0);
		if (fan_err) {
			pr_err("CPU fan could not be reset!\n");
		}
	}

	return zone_err ? zone_err : fan_err;
}

/**
 * Function to schedule the fan_control() function call to the common kernel-global
 * work queue.
 *
 * @retval   0  Scheduling successful
 * @retval  -1  Scheduling failed
 *
 * <b>Global variables</b>@n
 * @link interval interval @endlink <c>[in]</c>
 * @link pb6475fc_wq pb6475fc_wq @endlink <c>[in,out]</c>
 */
static inline int schedule_fan_control(void)
{
	schedule_delayed_work(&pb6475fc_wq, msecs_to_jiffies(interval));

	return (delayed_work_pending(&pb6475fc_wq) ? 0 : -1);
}


/* ************************************************************************** *
 *               Functions to access the Embedded Controller
 * ************************************************************************** */

/**
 * Function to get the current fan speed from the embedded controller.
 */
static inline int EC_get_fan_speed(u8 *cur_fan_speed)
{
	return ec_read(FRDCREG, cur_fan_speed);
}

/**
 * Function to set a fan speed to be controlled by the embedded controller.
 */
static inline int EC_set_fan_speed(u8 req_fan_speed)
{
	return ec_write(FTGCREG, req_fan_speed);
}

/**
 * Function to get the currently selected thermal zone.
 */
static inline int EC_get_thermal_zone(u8 *cur_thermal_zone)
{
	return ec_read(CRZNREG, cur_thermal_zone);
}

/**
 * Function to change the thermal zone to be monitored by the embedded controller.
 */
static inline int EC_set_thermal_zone(u8 req_thermal_zone)
{
	return ec_write(CRZNREG, req_thermal_zone);
}

/**
 * Function to get the current CPU temperature from the embedded controller.
 *
 * The selection of the right thermal zone has to be ensured in advanced via
 * EC_GetCurThermalZone() and, if required, EC_SetThermalZone().
*/
static inline int EC_get_CPU_temperature(u8 *cur_CPU_temp)
{
	return ec_read(DTMPREG, cur_CPU_temp);
}

/**
 * Function to pretend a fake temperature for the selected thermal zone to stop
 * the embedded controller of regulating the fan.
 */
static inline int EC_set_zone_temperature(u8 req_zone_temp)
{
	return ec_write(TEMPREG, req_zone_temp);
}


/* ************************************************************************** *
 *       Functions for fan controlling and fan/temperature observation
 * ************************************************************************** */

/**
 * Main controlling function that switches the CPU fan speed depending on the CPU
 * temperature, with respect to the thresholds given in the fan_upshift_temps[]
 * and fan_downshift_temps[] tables.
 *
 * In case the CPU temperature reaches a critical level or an error is detected
 * within fan speed and temperature sensing, the controlling will be disabled.
 *
 * @param[in]  unused  Parameter demanded for a work queue function but not
 *                     required by fan_control().
 *
 * <b>Global variables</b>@n
 * @link current_fan_state current_fan_state @endlink <c>[in]</c>
 * @link fan_controlling fan_controlling @endlink <c>[out]</c>
 * @link fan_cycles fan_cycles @endlink <c>[in]</c>
 * @link fan_downshift_temps fan_downshift_temps @endlink <c>[in]</c>
 * @link fan_upshift_temps fan_upshift_temps @endlink <c>[in]</c>
 * @link verbose verbose @endlink <c>[in]</c>
 */
static void fan_control(struct work_struct *unused)
{
	int fan_speed_err;
	int temp_sens_err;
	u8 verified_temp;

	/* Verify temperature */
	temp_sens_err = verify_temperature(get_CPU_temperature(), &verified_temp);

	/* Verify fan speed */
	fan_speed_err = verify_fan_speed(fan_cycles[current_fan_state]);

	/* Check, if the controlling has to be switched off due to temperature sensing
	 * and fan speed error.
	 */
	if (temp_sens_err && fan_speed_err) {
		pr_err("CPU temperature sensor and CPU fan failed, controlling disabled!\n");
		goto error;
	}

	/* Check, if the controlling has to be switched off due to high temperature */
	if (verified_temp >= CRITICALTEMP)
	{
		pr_crit("CPU temperature reached critical level (%u°C), controlling disabled!\n",
				verified_temp);
		goto error;
	}

	/* Check, if the fan speed has to be increased */
	if ((current_fan_state < MAXFANSTATE) && (verified_temp >= fan_upshift_temps[current_fan_state])) {
		if (set_fan_state(current_fan_state + 1)) {
			pr_warn("Fan state could not be upshifted to %u!\n", (current_fan_state + 1));
		} else {
			if (verbose) pr_info("Fan state upshifted to %u\n", current_fan_state);
		}
	}

	/* Check, if the fan speed can be decreased */
	if ((current_fan_state > 0) && (verified_temp <= fan_downshift_temps[current_fan_state])) {
		if (set_fan_state(current_fan_state - 1)) {
			pr_warn("Fan state could not be downshifted to %u!\n", (current_fan_state - 1));
		} else {
			if (verbose) pr_info("Fan state downshifted to %u\n", current_fan_state);
		}
	}

	/* Schedule fan_control() to be invoked again after 'interval' time */
	if (schedule_fan_control()) {
		pr_err("Scheduling fan control failed!\n");
		goto error;
	}

	return;

error:
	/* Ressign back fan control to EC in error case */
	if (reassign_fan_control()) {
		pr_emerg("Fan control reassigned to EC failed!\n");
	} else {
		pr_notice("Fan control assigned back to EC\n");
	}

	fan_controlling = 0;

	return;
}

/**
 * Function that gets a (raw) temperature value from the EC and prepares and
 * verifies it for fan controlling.
 *
 * As long as the EC provides valid temperature signal, the temperature is
 * simply low-pass filtered.
 *
 * In case the EC cannot deliver a valid temperature signal, the temperature
 * will be set to an emergency temperature level after a tolerance time.
 *
 * @param[in]   CPU_temp       Current measured CPU temperature provided by the EC.
 * @param[out]  verified_temp  Verified temperature for fan controlling.
 *
 * @retval  0  No error.
 * @retval  1  Temperature sensor error is detected.
 *
 * <b>Global variables</b>@n
 * @link reset_temp_verification reset_temp_verification @endlink <c>[in,out]</c>
 */
static int verify_temperature(u8 CPU_temp, u8 *verified_temp)
{
	static unsigned long timestamp;
	static int temp_sens_err;
	static u8 last_valid_temp;

	/* Function statics have to be reset while module initialization or after
	 * suspend to RAM/disk.
	 */
	if (reset_temp_verification == 1) {
		reset_temp_verification = 0;
		timestamp = jiffies;
		temp_sens_err = 0;
		last_valid_temp = EMERGENCYTEMP;
	}

	/* Check, if EC has delivered a valid temperature signal */
	if (CPU_temp == 255) {
		/* Wait a tolerance time until an error is detected. */
		if (jiffies_to_msecs(jiffies - timestamp) < VERIFYTEMPDELAY) goto exit;

		if (temp_sens_err == 0) {
			pr_warn("Temperature sensor failed, temperature set to emergency temperature (%u°C)\n",
					EMERGENCYTEMP);
			last_valid_temp = EMERGENCYTEMP;
			temp_sens_err = 1;
		}
	} else {
		/* Check, if error has to be reset */
		if (temp_sens_err == 1) {
			pr_warn("Thermal zone re-emerged, currently detected zone temperature %u°C\n",
					CPU_temp);
			last_valid_temp = CPU_temp;
			temp_sens_err = 0;
		}

		/* The zone temperature is filtered a bit to restrain some high dynamic temperature sensors */
		last_valid_temp = (CPU_temp >= last_valid_temp) ?
					((CPU_temp + last_valid_temp + 1) / 2) :
					((CPU_temp + last_valid_temp - 1) / 2);

		/* Remember timestamp of last function call with valid temperature signal */
		timestamp = jiffies;
	}

exit:
	*verified_temp = last_valid_temp;

	if (verbose > 1)
		pr_info("\t\tVerified temperature = %u°C, %serror\n",
				*verified_temp, temp_sens_err ? "" : "no ");

	return temp_sens_err;
}

/**
 * Function that compares the measured fan speed with the fan speed currently
 * requested by the controlling function.
 *
 * After a start-up time, the measured fan speed is read from the EC. If this
 * fails, a fan speed error is detected immediately, otherwise the measured and
 * requested fan speed are compared and if the spread between both values is to
 * high, an error will be detected.
 *
  @param[in]  req_fan_speed  Requested fan speeded by the controlling function
 *
 * @retval  0  No error.
 * @retval  1  Fan Speed error is detected.
 *
 * <b>Global variables</b>@n
 * @link current_fan_state current_fan_state @endlink <c>[in]</c>
 * @link reset_speed_verification reset_speed_verification @endlink <c>[in,out]</c>
 * @link verbose verbose @endlink <c>[in]</c>
 */
static int verify_fan_speed(u8 req_fan_speed)
{
	static unsigned long timestamp;
	static int fan_speed_err;
	static u8 prev_fan_state;
	u8 cur_fan_speed;
	u8 speed_deviation;

	/* Function statics have to be reset while module initialization or after
	 * suspend to RAM/disk, or if the fan state has changed.
	 */
	if ((reset_speed_verification == 1) || (prev_fan_state != current_fan_state)) {
		reset_speed_verification = 0;
		timestamp = jiffies;
		fan_speed_err = 0;
		prev_fan_state = current_fan_state;
	}

	/* No observation if the fan is switched off, an error has been detected
	 * already, or as long as a tolerance time hasn't elapsed.
	 */
	if ((current_fan_state == 0) || (fan_speed_err == 1) ||
			(jiffies_to_msecs(jiffies - timestamp) <= VERIFYSPEEDDELAY))
		goto exit;

	/* Get the currently measured fan speed via EC*/
	if(EC_get_fan_speed(&cur_fan_speed)) {
		pr_warn("CPU fan failed, could not get current fan speed!\n");
		fan_speed_err = 1;
		goto exit;
	}

	/* Calculate the absolute difference between the requested and the measured
	 * fan speed.
	 */
	speed_deviation = (cur_fan_speed > req_fan_speed) ?
				(cur_fan_speed - req_fan_speed) :
				(req_fan_speed - cur_fan_speed);

	/* Check, if the speed deviation between the requested and the measured
	 * fan speed is to high.
	 */
	if (speed_deviation > MAXSPEEDDEVIATION) {
		pr_warn("CPU fan failed, spread between current (%u) and requested (%u) fan speed to high!\n",
				cur_fan_speed, req_fan_speed);
		fan_speed_err = 1;
	}

	if (verbose > 1)
		pr_info("\t\tSpread between current and requested fan speed = %urpm, %serror\n",
				(speed_deviation*100), fan_speed_err ? "" : "no ");

exit:
	return fan_speed_err;
}

/**
 * Function to get the current CPU temperature in °C from the EC.
 *
 * @return Current CPU temperature in °C, or 255 in error case.
 *
 * <b>Global variables</b>@n
 * @link verbose verbose @endlink <c>[in]</c>
 */
static u8 get_CPU_temperature(void)
{
	int err;
	u8 thermal_zone;
	u8 CPU_temp;

	/* Check, if the correct thermal zone is selected. */
	err = EC_get_thermal_zone(&thermal_zone);
	if (err) {
		pr_err("Thermal zone could not be read!\n");
		CPU_temp = 255;
		goto exit;
	}

	/* If necessary, explicitly set the thermal zone to CPU zone. */
	if (thermal_zone != CPUZ) {
		err = EC_set_thermal_zone(CPUZ);
		if (err) {
			pr_err("Thermal zone could not be set to CPUZ!\n");
			CPU_temp = 255;
			goto exit;
		}
	}

	/* Read out the current zone temperature via the embedded controller */
	err = EC_get_CPU_temperature(&CPU_temp);
	if (err){
		pr_err("CPU temperature could not be read!\n");
		CPU_temp = 255;
	}

exit:
	if (verbose > 2)
		pr_info("\t\tCurrent CPU temperature = %u°C\n", CPU_temp);

	return CPU_temp;
}

/**
 * Function to change the fan state according to the request state.
 *
 * Requested fan states between 0 (no cooling) and MAXFANSTATE (maximum cooling)
 * are processed directly, values above MAXFANSTATE are limited to MAXFANSTATE.
 *
 * Depending on the requested fan state the associated speed value, given in
 * fan_cycles[] table, are passed to the EC for controlling.
 *
 * @param[in] req_fan_state - Requested fan state to be set.
 *
 * <b>Global variables</b>@n
 * @link current_fan_state current_fan_state @endlink <c>[in,out]</c>
 * @link fan_cycles fan_cycles @endlink <c>[in]</c>
 * @link verbose verbose @endlink <c>[in]</c>
 */
static int set_fan_state(u8 req_fan_state)
{
	int err;
	u8 tmp_state;

	tmp_state = (req_fan_state > MAXFANSTATE) ? MAXFANSTATE : req_fan_state;

	err = EC_set_fan_speed(fan_cycles[tmp_state]);

	/* In error case the fan state is not changed and remains at its current
	 * position.
	 */
	if (err) {
		pr_err("Fan state could not be set to %u!\n", tmp_state);
	} else {
		current_fan_state = tmp_state;
		if (verbose > 2)
			pr_info("\t\tFan state set to %u\n", current_fan_state);
	}

	return err;
}


/* ************************************************************************** *
 *                    Module load and unload functions
 * ************************************************************************** */

/**
 * Module initialization function that sets up the platform interface, kernel
 * timer and fan controlling.
 *
 * <b>Global variables</b>@n
 * @link fan_controlling fan_controlling @endlink <c>[out]</c>
 * @link reset_speed_verification reset_speed_verification @endlink <c>[out]</c>
 * @link reset_temp_verification reset_temp_verification @endlink <c>[out]</c>
 */
static int __init pb6475fc_init(void)
{
	int err;

	err = check_system("Hewlett-Packard", "HP ProBook 6475b");
	if (err) {
		pr_err("System hardware not supported!\n");
		goto error;
	}

	err = register_platform();
	if (err) {
		pr_err("Platform registration failed!\n");
		goto error;
	}

	err = takeover_fan_control();
	if (err) {
		pr_err("Fan control taken over form EC failed!\n");
		unregister_platform();
		goto error;
	} else {
		pr_notice("Fan control taken over from EC\n");
	}

	err = schedule_fan_control();
	if (err) {
		pr_err("Scheduling fan control failed!\n");

		if (reassign_fan_control())
			pr_emerg("Fan control reassigned to EC failed!\n");
		else
			pr_notice("Fan control reassigned to EC\n");

		unregister_platform();
		goto error;
	}

	/* Reset verification functions */
	reset_speed_verification = 1;
	reset_temp_verification = 1;

	/* Module initialization without error, fan controlling admitted. */
	fan_controlling = 1;

error:
	return err;
}

/**
 * Module exit function that stops fan controlling and cancels the platform
 * interface.
 *
 * <b>Global variables</b>@n
 * @link fan_controlling fan_controlling @endlink <c>[out]</c>
 * @link pb6475fc_wq pb6475fc_wq @endlink <c>[out]</c>
 */
static void __exit pb6475fc_exit(void)
{
	/* Reassigning fan control is not needed if the processing has stopped
	 * due to error or couldn't started at all while module initialization.
	 */
	if (fan_controlling) {
		cancel_delayed_work_sync(&pb6475fc_wq);

		if (reassign_fan_control())
			pr_emerg("Fan control reassigned to EC failed!\n");
		else
			pr_notice("Fan control reassigned to EC\n");

		fan_controlling = 0;
	}

	unregister_platform();

	return;
}


/* ************************************************************************** *
 *                        Module specific bound macros
 * ************************************************************************** */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Marcel Schlottmann");
MODULE_DESCRIPTION("HP ProBook6475b fan driver");
MODULE_VERSION("2016-08-23");

module_init(pb6475fc_init);
module_exit(pb6475fc_exit);
